import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {WoodBlockToysApiService} from './service/WoodBlockToysApiService';
import {RouterModule, Routes} from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { UserComponent } from './component/user-components/user/user.component';
import { ListUsersComponent } from './component/user-components/list-users/list-users.component';
import {HttpClientModule} from '@angular/common/http';
import { CreateUserComponent } from './component/user-components/create-user/create-user.component';
import {FormsModule} from '@angular/forms';
import { BlocsComponent } from './component/blocs-components/blocs/blocs.component';
import { ShowBlocsComponent } from './component/blocs-components/show-blocs/show-blocs.component';
import { InfosBlocsComponent } from './component/blocs-components/infos-blocs/infos-blocs.component';

const appRoutes: Routes = [
  { path: 'user', component: ListUsersComponent } ,
  { path: 'create_user', component: CreateUserComponent } ,
  { path: '', component: HomeComponent } ,
  { path: 'blocs', component: BlocsComponent } ,
  { path: 'panier_blocs', component: InfosBlocsComponent } ,


];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserComponent,
    ListUsersComponent,
    CreateUserComponent,
    BlocsComponent,
    ShowBlocsComponent,
    InfosBlocsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes),
    FormsModule,
  ],
  providers: [
    WoodBlockToysApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
