import {Component, Input, OnInit} from '@angular/core';
import {BlocModel} from '../../../model/Bloc.model';

@Component({
  selector: 'app-show-blocs',
  templateUrl: './show-blocs.component.html',
  styleUrls: ['./show-blocs.component.css']
})
export class ShowBlocsComponent implements OnInit {
  @Input() blocs: BlocModel;

  constructor() { }

  ngOnInit() {
  }

}
