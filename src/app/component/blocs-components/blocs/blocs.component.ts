import {Component, Input, OnInit} from '@angular/core';
import {WoodBlockToysApiService} from '../../../service/WoodBlockToysApiService';
import {BlocModel} from '../../../model/Bloc.model';

@Component({
  selector: 'app-blocs',
  templateUrl: './blocs.component.html',
  styleUrls: ['./blocs.component.css']
})
export class BlocsComponent implements OnInit {

  @Input() blocs: BlocModel;

  private hauteur: string;
  private base: string;
  private couleur: string;
  private finition: string;
  private typeBois: string;

  constructor(private woodBlockToysApiService: WoodBlockToysApiService) { }

  ngOnInit() {
  }

  getHauteur() {
    console.log(this.hauteur);
    return this.hauteur;
  }

  getBase() {
    console.log(this.base);
    return this.base;
  }

  getCouleur() {
    console.log(this.couleur);
    return this.couleur;
  }

  getFinition() {
    console.log(this.finition);
    return this.finition;
  }

  getTypeBois() {
    console.log(this.typeBois);
    return this.typeBois;
  }
  postNewBloc() {
    const newBloc = new BlocModel
    (this.hauteur, this.base, this.couleur, this.finition, this.typeBois);
    this.woodBlockToysApiService.createBlocApi(newBloc).subscribe();
  }
}
