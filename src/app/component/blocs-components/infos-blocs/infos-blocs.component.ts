import { Component, OnInit } from '@angular/core';
import {BlocModel} from '../../../model/Bloc.model';
import {WoodBlockToysApiService} from '../../../service/WoodBlockToysApiService';

@Component({
  selector: 'app-infos-blocs',
  templateUrl: './infos-blocs.component.html',
  styleUrls: ['./infos-blocs.component.css']
})
export class InfosBlocsComponent implements OnInit {

  listeBlocs: BlocModel[] = [];


  constructor( private woodBlockToysApiService: WoodBlockToysApiService) { }

  ngOnInit() {
    this.woodBlockToysApiService.getBlocApi().subscribe(blocApi => {
      console.log(this.listeBlocs);
      this.listeBlocs = blocApi;
      console.log(blocApi);
  } );
}
}
