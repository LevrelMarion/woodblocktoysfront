import { Component, OnInit } from '@angular/core';
import {UserModel} from '../../../model/User.model';
import {WoodBlockToysApiService} from '../../../service/WoodBlockToysApiService';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  listOfUsers: UserModel[] = [];

  constructor(private woodBlockToysApiService: WoodBlockToysApiService) { }

  ngOnInit() {
    this.woodBlockToysApiService.getUserApi().subscribe( woodBlockToysApi => {
      this.listOfUsers = woodBlockToysApi;
      console.log(this.listOfUsers);
    });
  }

}
