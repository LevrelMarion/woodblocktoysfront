import {Component, OnInit} from '@angular/core';
import {WoodBlockToysApiService} from '../../../service/WoodBlockToysApiService';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  nom: string;
  prenom: string;
  adressePostale: string;
  eMail: string;
  motDePasse: string;
  numTel: string;

  constructor(private woodBlockToysApiService: WoodBlockToysApiService) {
  }

  ngOnInit() {
  }

  getNom() {
    return this.nom;
  }

  getPrenom() {
    return this.prenom;
  }

  getAdressePostale() {
    return this.adressePostale;
  }

  geteMail() {
    return this.eMail;
  }

  getMotDePasse() {
    return this.motDePasse;
  }

  getNumTel() {
    return this.numTel;
  }

  onSubmit(result: NgForm) {
   // const results = result.value;
   // console.log(result.value.prenom);
   // const newUser = new UserModel(result.value.name, result.value.prenom,
   // result.value.adressePostale, result.value.eMail, result.value.motDePasse, result.value.numTel);
    this.woodBlockToysApiService.createUserApi(result.value).subscribe();
  }


}
