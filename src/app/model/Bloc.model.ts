export class BlocModel {
  hauteur: string;
  base: string;
  couleur: string;
  finition: string;
  typeBois: string;


  constructor(hauteur: string, base: string, couleur: string, finition: string, typeBois: string) {
    this.hauteur = hauteur;
    this.base = base;
    this.couleur = couleur;
    this.finition = finition;
    this.typeBois = typeBois;
  }
}
