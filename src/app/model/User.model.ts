export class UserModel {
  nom: string;
  prenom: string;
  adressePostale: string;
  eMail: string;
  motDePasse: string;
  numTel: string;


  constructor(nom: string, prenom: string, adressePostale: string, eMail: string, motDePasse: string, numTel: string) {
    this.nom = nom;
    this.prenom = prenom;
    this.adressePostale = adressePostale;
    this.eMail = eMail;
    this.motDePasse = motDePasse;
    this.numTel = numTel;
  }
}
