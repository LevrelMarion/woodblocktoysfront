import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserModel} from '../model/User.model';
import {BlocModel} from '../model/Bloc.model';

@Injectable()
export class WoodBlockToysApiService {

  constructor(private http: HttpClient) {
  }
  getUserApi(): Observable<UserModel[]> {
    return this.http.get<UserModel[]>('http://localhost:8080/user');
  }

  createUserApi(bodyUser: UserModel): Observable<UserModel> {
    console.log(this.http.post<UserModel>('http://localhost:8080/user', bodyUser));
    return this.http.post<UserModel>('http://localhost:8080/user', bodyUser);
  }

  getBlocApi(): Observable<BlocModel[]> {
    console.log(this.http.get<BlocModel[]>('http://localhost:8080/blocs'));
    return this.http.get<BlocModel[]>('http://localhost:8080/blocs');
  }
  createBlocApi(bodyBloc: BlocModel): Observable<BlocModel> {
    return this.http.post<BlocModel>('http://localhost:8080/blocs', bodyBloc);
  }
}
